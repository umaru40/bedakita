@extends('layout.MainLayout')

@section('content-MainLayout')
<section>
  @yield('content-Layout')
</section>
@endsection
