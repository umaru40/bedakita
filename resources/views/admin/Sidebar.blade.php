@extends('admin.HeadLayout')
@section('menu')
  <aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ session('foto') }}" class="img-circle" alt="{{ session('username') }}">
      </div>
      <div class="pull-left info">
        <p>{{ session('nama_lengkap') }}</p>
        {{ session('status') }}
      </div>
    </div>

    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li class="{{ $dashboard }} treeview">
        <a href="{{ url('/admin.html') }}"><i class="fa fa-home"></i> <span>Dashboard</span></a>
      </li>
      <li class="{{ $produk.$kategori }} treeview">
        <a href="#">
          <i class="fa fa-dropbox"></i>
          <span>Produk</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="{{ $produk }}"><a href="{{ url('/admin/productlist.html') }}"><i class="fa fa-circle-o"></i> Daftar Produk</a></li>
          <li class="{{ $kategori }}"><a href="{{ url('/admin/categorylist.html') }}"><i class="fa fa-circle-o"></i> Kategori</a></li>
        </ul>
      </li>
      <li class="{{ $member }}">
        <a href="{{ url('/admin/memberlist.html') }}">
          <i class="fa fa-users"></i> <span>Member</span>
        </a>
      </li>
      <li class="{{ $slide }}">
        <a href="{{ url('/admin/slidelist.html') }}">
          <i class="fa fa-picture-o"></i> <span>Slide</span>
        </a>
      </li>
      <li class="{{ $profil.$password }} treeview">
        <a href="#">
          <i class="fa fa-gear"></i>
          <span>Pengaturan Akun</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="{{ $profil }}"><a href="{{ url('/admin/profile/'.session('username').'.html') }}"><i class="fa fa-circle-o"></i> Profil</a></li>
          <li class="{{ $password }}"><a href="{{ url('/admin/password.html') }}"><i class="fa fa-circle-o"></i> Password</a></li>
        </ul>
      </li>
      <li>
        <a href="{{ url('/signout') }}">
          <i class="fa fa-power-off"></i> <span>Sign Out</span>
        </a>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>

  @yield('content')
@endsection
