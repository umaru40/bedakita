@extends('admin.Sidebar', ['dashboard' => '',
                            'produk' => '',
                            'kategori' => '',
                            'member' => '',
                            'slide' => '',
                            'profil' => '',
                            'password' => 'active'])

@section('title')
  Password Form
@endsection

@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Password
      <small>Form</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#"><i class="fa fa-gear"></i> Pengaturan Akun</a></li>
      <li class="active">Password</li>
    </ol>
  </section>

  <section class="content">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form Ganti Password</h3>
            </div>

            {!! Form::open(['url' => 'admin/memberform/gantipassword', 'method' => 'post', 'class' => 'form-horizontal']) !!}
              <div class="box-body">
                <div class="form-group">
                  {!! Form::label('passwordlama', 'Password Lama', ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-9">
                    {!! Form::password('passwordlama', ['class' => 'form-control', 'placeholder' => 'Password Lama', 'autofocus' => 'true', 'required' => 'true']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! Form::label('passwordbaru', 'Password Baru', ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-9">
                    {!! Form::password('passwordbaru', ['class' => 'form-control', 'placeholder' => 'Password Baru', 'required' => 'true']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! Form::label('passwordulangbaru', 'Ulangi Password Baru', ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-9">
                    {!! Form::password('passwordulangbaru', ['class' => 'form-control', 'placeholder' => 'Ulangi Password Baru','required' => 'true']) !!}
                  </div>
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
  </section>
</div>
<script type="text/javascript">
  $(function(){
    $('.select2').select2();
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue'
    })
  });
</script>
@endsection
