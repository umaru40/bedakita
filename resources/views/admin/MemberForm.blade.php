@extends('admin.Sidebar', ['dashboard' => '',
                            'produk' => '',
                            'kategori' => '',
                            'member' => $member,
                            'slide' => '',
                            'profil' => $profil,
                            'password' => ''])

@section('title')
  {{ $jenis }} Form
@endsection

@section('content')

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      {{ $jenis }}
      <small>Form</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      @if( $jenis == "Member")
      <li><a href="{{ url('/admin/memberlist.html') }}"><i class="fa fa-users"></i> Member</a></li>
      <li class="active">Form Member</li>
      @else
      <li><a href="#"><i class="fa fa-gear"></i> Pengaturan Akun</a></li>
      <li class="active">Profil</li>
      @endif
    </ol>
  </section>

  <section class="content">
     @if (Session::has('info') or count($errors) > 0)
    <div class="callout callout-{{ session('kelas') }} hidden" id="information">
        <h4>Informasi</h4>
        {!! session('info') !!}
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
    </div>
    @endif

    
      <div class="row">
        <div class="col-md-4">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Foto Profil</h3>
            </div>

            <form role="form">
              <div class="box-body">
                <div class="form-group text-center">
                  <img class="profile-user-img img-responsive img-thumbnail" id="foto_memberx" src="{{ $foto }}" alt="{{ $username }}">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Perbarui Foto Profil</label>

                  <input type="file" id="foto_member">
                </div>
              </div>

              <div class="cropfoto_member" style="width:350px; margin-top:30px"></div>

            </form>
          </div>
        </div>

        <div class="col-md-8">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Info {{ $jenis }}</h3>
            </div>

            {!! Form::open(['url' => 'admin/memberform/store.html', 'method' => 'post','id' => 'frm_member', 'class' => 'form-horizontal']) !!}
              <div class="box-body">
                <div class="form-group">
                  {!! Form::label('username', 'Username', ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-9">
                    {!! Form::text('username', $username, ['class' => 'form-control','id' => 'username', 'placeholder' => 'Username', 'autofocus' => 'true', 'required' => 'true', 'maxlength' => 15]) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! Form::label('nama_lengkap', 'Nama Lengkap', ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-9">
                    {!! Form::text('nama_lengkap', $nama_lengkap, ['class' => 'form-control', 'placeholder' => 'Nama Lengkap']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! Form::label('nama_toko', 'Nama Toko', ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-9">
                    {!! Form::text('nama_toko', $nama_toko, ['class' => 'form-control', 'placeholder' => 'Nama Toko', 'autofocus' => 'true']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! Form::label('email', 'Email', ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-9">
                    {!! Form::email('email', $email, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! Form::label('tgl_lahir', 'Tanggal Lahir', ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-9">
                    {!! Form::date('tgl_lahir', $tgl_lahir, ['class' => 'form-control', 'placeholder' => 'Tanggal Lahir']) !!}
                  </div>
                </div>
                <div class="form-group" >
                  {!! Form::label('jenis_kelamin', 'Jenis Kelamin', ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-4">
                    {!! Form::radio('jenis_kelamin', '1', true, ['class' => 'flat-red']) !!} Laki Laki
                  </div>
                  <div class="col-sm-5">
                    {!! Form::radio('jenis_kelamin', '2', false, ['class' => 'flat-red']) !!} Perempuan
                  </div>
                </div>
                <div class="form-group">
                  {!! Form::label('no_hp', 'No HP', ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-9">
                    {!! Form::text('no_hp', $no_hp, ['class' => 'form-control', 'placeholder' => 'No HP', 'onkeypress' => 'return numeric(event);', 'maxlength' => 15]) !!}
                  </div>
                </div>
                <div class="form-group" >
                  {!! Form::label('alamat_member', 'Alamat Member', ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-9">
                    {!! Form::textarea('alamat_member', $alamat_pengguna, ['class' => 'form-control', 'placeholder' => 'Alamat Member', 'rows' => 3]) !!}
                  </div>
                </div>
                <div class="form-group" >
                  {!! Form::label('alamat_toko', 'Alamat Toko', ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-9">
                    {!! Form::textarea('alamat_toko', $alamat_toko, ['class' => 'form-control', 'placeholder' => 'Alamat Toko', 'rows' => 3]) !!}
                  </div>
                </div>
                <div class="form-group" >
                  {!! Form::label('status', 'Status', ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-9">
                    {!! Form::select('status', ['Admin' => 'Admin', 'Member' => 'Member'], $status, ['class' => 'form-control select2', 'style' => 'width: 100%;']) !!}
                  </div>
                </div>
              </div>

              <div class="box-footer">
                <div id="fbform_member"></div>
                <input type="hidden" id="foto" name="foto">
                <input type="hidden" name="qry" value="{{ $qry }}">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
  </section>
</div>
<script type="text/javascript">
  $(function(){
    $('.select2').select2();
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue'
    })
  });
</script>
@endsection
