@extends('layout.PenjualanLayout', ['beranda' => '',
                            'produk' => 'active',
                            'bedag' => ''
                            ])
@section('title')
Produk -
@endsection

@section('slide')
@endsection

@section('content-PenjualanLayout')
<?php
	$url   = url()->full();
	if(strpos($url, "?") > 0){
		$url = substr($url, 0, strpos($url, "?"));
	}
?>
<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="left-sidebar">
          <h2>Kategori</h2>
          <div class="panel-group category-products" id="accordian">
            @foreach($categorie as $category)
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"><a href="{{ $url.'?kategori='.$category->nama_kategori }}">{{ $category->nama_kategori }}</a></h4>
              </div>
            </div>
            @endforeach
          </div>

          <div class="brands_products">
            <h2>Bedag</h2>
            <div class="brands-name">
              <ul class="nav nav-pills nav-stacked">
                @foreach($bedag as $shop)
                  <li><a href="{{ url('produk/'.$shop->username) }}"> <span class="pull-right">({{ $shop->banyak }})</span>{{ $shop->nama_toko }}</a></li>
                @endforeach
              </ul>
            </div>
          </div>

        </div>
      </div>

      <div class="col-sm-9 padding-right">
        <div class="features_items"><!--features_items-->
          <h2 class="title text-center">{{ $nama_bedag }}</h2>
          <div class="row">
            @if(count($product) == 0)
            <div class="col-sm-12">
              <center><div class="alert alert-info" style="width: 95%;">
                @if(isset($_GET['cari']))
                  Dari pencarian <strong>"{{ $_GET['cari'] }}"</strong>,
                @endif
                Tidak ada produk yang bisa ditampilkan</div></center>
            </div>
            @endif
            @foreach($product as $product)
            <div class="col-sm-4">
              <div class="product-image-wrapper">
                <div class="single-products">
                  <div class="productinfo text-center">
                    <div class="foto-produk">
											<img width="100%" src="{{ $foto[$product->kode_produk] }}" alt="{{ $product->nama_produk }}" />
										</div>
										<h2>Rp.  {{ number_format($product->harga_produk,0,'','.') }},-</h2>
										<p class="teks_produk">{{$product->nama_produk}}</p>
										<a href="{{ url('/detailproduk/'.$product->kode_produk) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Detail</a>
                  </div>
                  <div class="product-overlay">
                    <div class="overlay-content">
                      <h2>Rp.  {{ number_format($product->harga_produk,0,'','.') }},-</h2>
											<p class="teks_produk">{{$product->nama_produk}}</p>
											<a href="{{ url('/detailproduk/'.$product->kode_produk) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Detail</a>
                    </div>
                  </div>
                </div>
                <div class="choose">
                  <ul class="nav nav-pills nav-justified">
                    <li><a href="#"><i class="fa fa-home"></i>Bedag <b>{{$product->username}}</b></a></li>
                  </ul>
                </div>
              </div>
            </div>
            @endforeach


          </div>

          <ul class="pagination">
            <li class="active"><a href="">1</a></li>
            <li><a href="">2</a></li>
            <li><a href="">3</a></li>
            <li><a href="">&raquo;</a></li>
          </ul>
        </div><!--features_items-->
      </div>
    </div>
  </div>
</section>
@endsection
