@extends('layout.PenjualanLayout', ['beranda' => '',
                            'produk' => '',
                            'bedag' => 'active'
                            ])
@section('title')
	Daftar Bedag -
@endsection
<!-- content-penjualanLayout -->
@section('content-PenjualanLayout')
<section>
	<div class="container" id="content">
		<div class="row">
			<div class="col-sm-12 padding-right">

				<table id="list_produk" class="table table-striped table-bordered" style="width:100%">
			        <thead>
			            <tr>
			                <th>NAMA BEDAG</th>
			                <th>ALAMAT BEDAG</th>
			                <th>PEMILIK</th>
			                <th>CONTACT PERSON</th>
			                <th>LIHAT PRODUK</th>
			            </tr>
			        </thead>
			        <tbody>
						@foreach($daftarbedag as $bedag)
			            <tr>
			                <td>{{$bedag->nama_toko}}</td>
			                <td>{{$bedag->alamat_toko}}</td>
			                <td>{{$bedag->nama_lengkap}}</td>
			                <td>{{$bedag->no_hp}}</td>
			                <td><a href="{{url('/produk/'.$bedag->username)}}" class="btn btn-success"><i>Lihat Produk</i></a></td>
			            </tr>
						@endforeach
			        </tbody>
			        <tfoot>
			             <tr>
			                <th>NAMA BEDAG</th>
			                <th>ALAMAT BEDAG</th>
			                <th>PEMILIK</th>
			                <th>CONTACT PERSON</th>
											<th>LIHAT PRODUK</th>
			            </tr>
			        </tfoot>
			    </table>

			</div>
		</div>
	</div>
</section>
<!-- end content-penjualanLayout -->

@endsection
