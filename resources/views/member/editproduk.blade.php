@extends('layout.PenjualanLayout', ['beranda' => '',
                            'produk' => '',
                            'bedag' => ''
                            ])

@section('content-PenjualanLayout')
<section>

<div class="container">
	<div class="row">
		<div class="col-sm-12">

		<div class="category-tab shop-details-tab" style="margin:auto"><!--category-tab-->
			<div class="col-sm-12">
				<!-- <ul class="nav nav-tabs">
					<li ><a href="#details" data-toggle="tab">Biodata</a></li>
				</ul> -->
			</div>
			<div class="tab-content">
				<div class="tab-pane fade active in" id="details" >
					<div class="col-sm-12" style="margin-bottom:20px">
					<a href="{{ url('/member/'.Session::get('username')) }}" class="btn btn-success text-primary"><span class="fa fa-reply-all"></span><i> Kembali</i></a>
					</div>

					<div class="col-sm-4">
            <div class="row" id="photo_thumb">
						  <img id="img_editproduk" src="{{ $foto }}" class="img img-responsive " width="70%" alt="{{ $product->nama_produk }}">
            </div>
            <div class="col-sm-12">
  						<b>Edit Gambar Produk</b>
  						<input type="file" class="btn btn-primary" id="upload_imageEditProduk">
  						<input type="hidden" id="url_editprodukgbr" value="{{url('/edit_produkgbr')}}">
  						<div class="gbr_produk" style="width:350px; margin-top:30px"></div>
  						<div id="edit_produk"></div>
  						<button class="btn btn-primary" id="crop_gbrproduk">Unggah Gambar</button>
  					</div>
					</div>
					<div class="col-sm-8">

					<div class="table-responsive cart_info">
					{{Form::open(array('url'=>'simpan_editproduk','method'=>'post','id'=>'frm_editproduk'))}}
						<input type="hidden" name="kode_produk" id="kode_produk" value="{{$product->kode_produk}}">
						<table class="table table-condensed">
							<tbody>
								<tr>
									<td>
									</td>
								</tr>
								<tr >
									<td>Nama Produk</td>
									<td width="70%">
										{{Form::text('nama_produk',$product->nama_produk,array('class'=>'form-control','placeholder'=>'Nama Produk'))}}
										<div id="nama_produkError" class="text-danger"></div>
									</td>
								</tr>
								<tr>
									<td >Harga Produk</td>
									<td >
                    <div class="form-group">
                      <label class="sr-only" for="exampleInputAmount">Harga</label>
                      <div class="input-group">
                        <div class="input-group-addon">Rp.</div>
                        {{Form::text('harga_produk',$product->harga_produk,array('class'=>'form-control','placeholder'=>'Harga Produk', 'onkeypress' => 'return numeric(event)'))}}
                        <div class="input-group-addon">.-</div>
                      </div>
                    </div>
										<div id="harga_produkError" class="text-danger"></div>
									</td>
								</tr>
								<tr>
									<td>Kategori Produk</td>
									<td width="70%">
                    {{Form::select('kode_kategori',$kategori,$product->kode_kategori, ['class' => 'form-control select2', 'style' => 'width: 100%;'])}}
									<div id="kategori_produkError" class="text-danger"></div>
									</td>
								</tr>
								<tr>
									<td style="vertical-align: top;">Deskripsi Produk</td>
									<td width="70%">
										{{Form::textarea('deskripsi_produk',$product->deskripsi_produk,array('class'=>'form-control editor','cols'=>'50','rows'=>'3','id'=>'keterangan_produk'))}}
										<div id="deskripsi_produkError" class="text-danger"></div>
									</td>
								</tr>
								<tr>
									<td></td>
									<td>
										<div id="fb_editproduk" class="text-primary"></div>
										{{Form::submit('SIMPAN',array('class'=>'btn btn-primary'))}}
									</td>
								</tr>
							</tbody>
						</table>
					{{Form::close()}}

					</div>


					</div>

				</div>



			</div>
		</div><!--/category-tab-->



		</div>
	</div>
</div>




</section>


@endsection
