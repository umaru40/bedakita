<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Custom extends CI_Model{
	function photo($foto, $kriteria, $kode = ""){
		if(empty($kode)){
			$thumb = base_url('assets/images').'/'.$kriteria.'/'.$foto;
			$loc   = "./assets/images/".$kriteria.'/'.$foto;
		}else{
			$thumb = base_url('assets/images').'/'.$kriteria.'/'.$kode.'/'.$foto;
			$loc   = "./assets/images/".$kriteria.'/'.$kode.'/'.$foto;
		}

		if(!file_exists($loc)){
			$thumb = base_url('assets/images').'/'.$kriteria.'/default.png';
		}

		return $thumb;
	}

	function cek_nama_foto($foto, $kriteria, $kode = ""){
		if(empty($kode)){
			$thumb = "/".$kriteria.'/'.$foto;
			$loc   = "./assets/image/".$kriteria.'/'.$foto;
		}else{
			$thumb = "/".$kriteria.'/'.$kode.'/'.$foto;
			$loc   = "./assets/image/".$kriteria.'/'.$kode.'/'.$foto;
		}

		if(!file_exists($loc)){
			$thumb = '/'.$kriteria.'/no.jpg';
		}

		return $thumb;
	}

	function deleteimage($foto, $kriteria)
	{
		unlink("./assets/images/".$kriteria.'/'.$foto);
		return "Berhasil";
	}

	public function indo_bulan($bln) {
		$nm_bln = "";
		if ($bln == 1) {
            $nm_bln = "Januari";
        }
        elseif($bln == 2) {
            $nm_bln = "Februari";
        }
        elseif($bln == 3) {
            $nm_bln = "Maret";
        }
        elseif($bln == 4) {
            $nm_bln = "April";
        }
        elseif($bln == 5) {
            $nm_bln = "Mei";
        }
        elseif($bln == 6) {
            $nm_bln = "Juni";
        }
        elseif($bln == 7) {
            $nm_bln = "Juli";
        }
        elseif($bln == 8) {
            $nm_bln = "Agustus";
        }
        elseif($bln == 9) {
            $nm_bln = "September";
        }
        elseif($bln == 10) {
            $nm_bln = "Oktober";
        }
        elseif($bln == 11) {
            $nm_bln = "November";
        }
        else {
            $nm_bln = "Desember";
        }
        return $nm_bln;
	}

	function tanggal_indo($tanggal)
    {
        $bulan = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
        $split = explode('-', $tanggal);
        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    }

	function pagination($total, $base, $current_page){
		$per_page   = 8;

		if($total    <= $per_page){
			$pagination = "";
		}else{
			if(empty($current_page)){
				$current_page = 1;
			}

			$pagination = '<ul class="pagination">';
			$count      = ceil($total/$per_page);
			$two_back   = $current_page - 2;
			$two_front  = $current_page + 2;

			if(strpos($base, "?") > 0)
			{
				$base     = $base."&page=";
			}else{
				$base     = $base."?page=";
			}

			if($current_page > 1){
				$page       = $current_page - 1;
				$pagination = $pagination.'<li><a href="'.$base.$page.'">&laquo;</a></li>';
			}

			if($current_page > 3){
				$page       = $two_back - 1;
				$pagination = $pagination.'<li><a href="'.$base.$page.'">...</a></li>';
			}

			for ($i=$two_back; $i < $current_page; $i++) {
				if($i > 0){
					$pagination = $pagination.'<li><a href="'.$base.$i.'">'.$i.'</a></li>';
				}
			}

			$pagination = $pagination.'<li class="active"><a href="#">'.$i.'</a></li>';

			for ($i = ($current_page + 1); $i <= $two_front; $i++) {
				if($i <= $count){
					$pagination = $pagination.'<li><a href="'.$base.$i.'">'.$i.'</a></li>';
				}
			}

			if(($current_page + 3) < $count){
				$page       = $two_front + 1;
				$pagination = $pagination.'<li><a href="'.$base.$page.'">...</a></li>';
			}

			if($two_front < $count)
			{
				$pagination = $pagination.'<li><a href="'.$base.$count.'">'.$count.'</a></li>';
			}

			if($current_page < $count){
				$page       = $current_page + 1;
				$pagination = $pagination.'<li><a href="'.$base.$page.'">&raquo;</a></li>';
			}

			$pagination = $pagination.'</ul>';
		}

		return $pagination;
	}

	function upload_gambar($id, $location, $jenis)
	{
		/*$id = $this->input->post('ID');*/
		if(empty($id)){
			$this->db->order_by("ID", 'desc');
			$database = $this->db->get('fe_gallery');
			$id_last = $database->first_row()->ID;
			$id = $id_last + 1;
		}else{
			$ubah['Foto'] = $jenis.'_'.$id.$ext;

			$this->db->where('ID', $id);
			$this->db->update('fe_gallery', $ubah);
		}

		$ext = ".".pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
		$scan = scandir($location,SCANDIR_SORT_DESCENDING);
		$jum = count($scan);
		if ($jum==0) {
			$config['file_name'] = $jenis.'_'.$id.$ext;
		}elseif($jum>0) {
			$alamat = $location.$jenis.'_'.$id.$ext;
			unlink($alamat);
			$config['file_name'] = $jenis.'_'.$id.$ext;
		}

        $config['upload_path'] = $location;
        $config['allowed_types'] = 'gif|jpg|png|bmp';

        $this->load->library('upload',$config);
		if (!empty($_FILES['userfile']['name'])) {
	        if (!$this->upload->do_upload()) {
				return $this->upload->display_errors();
			}
			else {
				return $config['file_name'];
			}
		}
	}

	function encry($input){
		$hasil = md5(substr(md5(substr(md5($input),0,9).'b9726dc'.substr(md5($input),9)),0,27).'f213a09b'.substr(md5(substr(md5($input),0,9).'b9726dc'.substr(md5($input),9)),27));
		return $hasil;
	}
  function pin( $q ) {
      $cryptKey  = 'd6Yq8Br34Kd3S1gN3R6a28';
      $qEncoded  = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $q, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
      return( $qEncoded );
  }
  function unpin( $q ) {
      $cryptKey  = 'd6Yq8Br34Kd3S1gN3R6a28';
      $qDecoded  = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
      return( $qDecoded );
  }
	function unpinget( $pin ){
    	$kode = str_replace(" ", "+", $pin);
        $kode = $this->custom->unpin($kode);
        return $kode;
  }
}
