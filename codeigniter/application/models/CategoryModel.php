<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CategoryModel extends CI_Model
{

    public $table = 'categories';
    public $id = 'kode_kategori';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('kode_kategori,nama_kategori,created_at,updated_at');
        $this->datatables->from('categories');
        //add this line for join
        //$this->datatables->join('table2', 'categories.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('categorycontroller/read/$1'),'Read')." | ".anchor(site_url('categorycontroller/update/$1'),'Update')." | ".anchor(site_url('categorycontroller/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'kode_kategori');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('kode_kategori', $q);
	$this->db->or_like('nama_kategori', $q);
	$this->db->or_like('created_at', $q);
	$this->db->or_like('updated_at', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('kode_kategori', $q);
	$this->db->or_like('nama_kategori', $q);
	$this->db->or_like('created_at', $q);
	$this->db->or_like('updated_at', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file CategoryModel.php */
/* Location: ./application/models/CategoryModel.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-07-06 14:02:04 */
/* http://harviacode.com */