<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengguna extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('PenggunaModel');
        $this->load->library('form_validation');
	      $this->load->library('datatables');
    }

    public function resetpass()
    {
        if($this->session->userdata('status') == "Admin")
        {
          $username      = $this->uri->segment(3);
          $data_pengguna = $this->PenggunaModel->get_by_id($username);

          if(!$data_pengguna)
          {
            $session['kelas']   = "warning";
            $session['info']    = "Username tidak terdaftar!";
          }else{
            $data['password']   = $this->Custom->encry($username);
            $data['updated_at'] = date('Y-m-d H:i:s');
            $this->PenggunaModel->update($username, $data);

            $session['kelas']   = "info";
            $session['info']    = "Password <strong>".$username."</strong> berhasil direset! Password : ".$username;
          }
          $this->session->set_flashdata($session);
          redirect('admin/memberlist');
        }else{
          redirect('main');
        }
    }

    public function store()
    {
        $data['username']            = $this->input->post('username');
        $data['nama_lengkap']        = $this->input->post('nama_lengkap');
        $data['email']               = $this->input->post('email');
        $data['tgl_lahir']           = $this->input->post('tgl_lahir');
        $data['jenis_kelamin']       = $this->input->post('jenis_kelamin');
        $data['no_hp']               = $this->input->post('no_hp');
        $data['alamat_pengguna']     = $this->input->post('alamat_member');
        $data['alamat_toko']         = $this->input->post('alamat_toko');
        $data['status']              = $this->input->post('status');
        $data['nama_toko']           = $this->input->post('nama_toko');
        $data['password']            = $this->Custom->encry($this->input->post('username'));
        $data['foto']                = $this->input->post('foto');
        $qry                         = $this->input->post('qry');
        $current                     = $this->PenggunaModel->get_by_id($data['username']);

         if($data['foto']            == ''){
            $image_name              = "default.png";
          }else{
            $crop = $data['foto'];
            list($type, $crop)       = explode(';', $crop);
            list(, $crop)            = explode(',', $crop);
            $crop                    = base64_decode($crop);
            $image_name              = $data['username'].'.png';
            $path                    = base_url('assets') . "/images/pengguna/" . $image_name;
            file_put_contents($path, $crop);
          }

        if($qry == "new"){
          $data['foto']              = $image_name;

          if($data['username']       == $current['username'])
          {
            $feedback['status']      = 0;
            $feedback['pesan']       = "<div class='alert alert-warning'>Username sudah dipakai</div>";
          }else{
            $feedback['status']      = 1;
            $feedback['pesan']       = "<div class='alert alert-info'>Berhasil </div>";
            $this->PenggunaModel->insert($data);
            $this->session->set_flashdata('info', 'Data <strong>'.$data['username'].'</strong> berhasil disimpan.');
          }

        }else{
          if($data['foto']           ==''){
            $data['foto']            = $current['foto'];
          }else{
            $data['foto']            = $image_name;
          }
          $feedback['status']        = 1;
          $feedback['pesan']         = "<div class='alert alert-info'>Berhasil </div>";
          $data['updated_at']        = date('Y-m-d H:i:s');
          $this->PenggunaModel->update($data['username'], $data);
          $this->session->set_flashdata('info', 'Data <strong>'.$data['username'].'</strong> berhasil diperbarui.');
        }
        $feedback['url']     = url('admin/memberform/'.$data['username'].'.html');

        // return $feedback;
        redirect('admin/memberlist');
    }

    public function memberdelete()
    {
        if($this->session->userdata('status') == "Admin")
        {
          $username         = $this->uri->segment(3);
          $data_pengguna    = $this->PenggunaModel->get_by_id($username);
          $session['info']  = 'Data <strong>'.$data_pengguna->nama_lengkap.'</strong> telah dihapus!';
          $session['kelas'] = 'warning';

          $this->PenggunaModel->delete($username);
          $this->session->set_flashdata($session);
          redirect('admin/memberlist');
        }else{
          redirect('main');
        }
    }

    public function changepass()
    {
      if(!empty($this->session->userdata('status')))
      {
        $username           = $this->session->userdata('username');
        $data_pengguna      = $this->PenggunaModel->get_by_id($username);
        $pass_lama          = $this->Custom->encry($this->input->post('pass_lama'));
        $data['password']   = $this->Custom->encry($this->input->post('pass_baru'));
        $data['updated_at'] = date('Y-m-d H:i:s');

        if(!$data_pengguna)
        {
            echo "Username ".$username." tidak ditemukan!";
        }else{
            if($pass_lama == $data_pengguna->password)
            {
                $this->PenggunaModel->update($username, $data);
                echo "Password Anda berhasil diubah.";
            }else{
                echo "Password Anda salah! Tidak dapat mengubah password.";
            }
        }
      }else{
        redirect('main');
      }
    }

    public function updatefoto()
    {
      $data = $request->image;
      list($type, $data) = explode(';', $data);
      list(, $data)      = explode(',', $data);
      $data = base64_decode($data);
      $image_name= $id.'.png';
      $path = public_path() . "/images/pengguna/" . $image_name;

      if(file_put_contents($path, $data)){
          $this->PenggunaModel->update($this->session->userdata('username'), ['foto'=>$image_name]);
          $feedback['pesan']  = 'Berhasil';
          $feedback['foto']   = $image_name;
      }else{
          $feedback['pesan'] = 'Gagal';
      }
      $feedback['url']   = url('/member/'.$id);
      return $feedback;
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->PenggunaModel->json();
    }

    public function read($id)
    {
        $row = $this->PenggunaModel->get_by_id($id);
        if ($row) {
            $data = array(
		'username' => $row->username,
		'nama_lengkap' => $row->nama_lengkap,
		'jenis_kelamin' => $row->jenis_kelamin,
		'tgl_lahir' => $row->tgl_lahir,
		'no_hp' => $row->no_hp,
		'alamat_member' => $row->alamat_member,
		'email' => $row->email,
		'password' => $row->password,
		'nama_toko' => $row->nama_toko,
		'alamat_toko' => $row->alamat_toko,
		'status' => $row->status,
		'foto' => $row->foto,
		'created_at' => $row->created_at,
		'updated_at' => $row->updated_at,
	    );
            $this->load->view('penggunacontroller/penggunas_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penggunacontroller'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('penggunacontroller/create_action'),
	    'username' => set_value('username'),
	    'nama_lengkap' => set_value('nama_lengkap'),
	    'jenis_kelamin' => set_value('jenis_kelamin'),
	    'tgl_lahir' => set_value('tgl_lahir'),
	    'no_hp' => set_value('no_hp'),
	    'alamat_member' => set_value('alamat_member'),
	    'email' => set_value('email'),
	    'password' => set_value('password'),
	    'nama_toko' => set_value('nama_toko'),
	    'alamat_toko' => set_value('alamat_toko'),
	    'status' => set_value('status'),
	    'foto' => set_value('foto'),
	    'created_at' => set_value('created_at'),
	    'updated_at' => set_value('updated_at'),
	);
        $this->load->view('penggunacontroller/penggunas_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_lengkap' => $this->input->post('nama_lengkap',TRUE),
		'jenis_kelamin' => $this->input->post('jenis_kelamin',TRUE),
		'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
		'no_hp' => $this->input->post('no_hp',TRUE),
		'alamat_member' => $this->input->post('alamat_member',TRUE),
		'email' => $this->input->post('email',TRUE),
		'password' => $this->input->post('password',TRUE),
		'nama_toko' => $this->input->post('nama_toko',TRUE),
		'alamat_toko' => $this->input->post('alamat_toko',TRUE),
		'status' => $this->input->post('status',TRUE),
		'foto' => $this->input->post('foto',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
		'updated_at' => $this->input->post('updated_at',TRUE),
	    );

            $this->PenggunaModel->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('penggunacontroller'));
        }
    }

    public function update($id)
    {
        $row = $this->PenggunaModel->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('penggunacontroller/update_action'),
		'username' => set_value('username', $row->username),
		'nama_lengkap' => set_value('nama_lengkap', $row->nama_lengkap),
		'jenis_kelamin' => set_value('jenis_kelamin', $row->jenis_kelamin),
		'tgl_lahir' => set_value('tgl_lahir', $row->tgl_lahir),
		'no_hp' => set_value('no_hp', $row->no_hp),
		'alamat_member' => set_value('alamat_member', $row->alamat_member),
		'email' => set_value('email', $row->email),
		'password' => set_value('password', $row->password),
		'nama_toko' => set_value('nama_toko', $row->nama_toko),
		'alamat_toko' => set_value('alamat_toko', $row->alamat_toko),
		'status' => set_value('status', $row->status),
		'foto' => set_value('foto', $row->foto),
		'created_at' => set_value('created_at', $row->created_at),
		'updated_at' => set_value('updated_at', $row->updated_at),
	    );
            $this->load->view('penggunacontroller/penggunas_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penggunacontroller'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('username', TRUE));
        } else {
            $data = array(
		'nama_lengkap' => $this->input->post('nama_lengkap',TRUE),
		'jenis_kelamin' => $this->input->post('jenis_kelamin',TRUE),
		'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
		'no_hp' => $this->input->post('no_hp',TRUE),
		'alamat_member' => $this->input->post('alamat_member',TRUE),
		'email' => $this->input->post('email',TRUE),
		'password' => $this->input->post('password',TRUE),
		'nama_toko' => $this->input->post('nama_toko',TRUE),
		'alamat_toko' => $this->input->post('alamat_toko',TRUE),
		'status' => $this->input->post('status',TRUE),
		'foto' => $this->input->post('foto',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
		'updated_at' => $this->input->post('updated_at',TRUE),
	    );

            $this->PenggunaModel->update($this->input->post('username', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('penggunacontroller'));
        }
    }

    public function delete($id)
    {
        $row = $this->PenggunaModel->get_by_id($id);

        if ($row) {
            $this->PenggunaModel->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('penggunacontroller'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penggunacontroller'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('nama_lengkap', 'nama lengkap', 'trim|required');
	$this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required');
	$this->form_validation->set_rules('tgl_lahir', 'tgl lahir', 'trim|required');
	$this->form_validation->set_rules('no_hp', 'no hp', 'trim|required');
	$this->form_validation->set_rules('alamat_member', 'alamat member', 'trim|required');
	$this->form_validation->set_rules('email', 'email', 'trim|required');
	$this->form_validation->set_rules('password', 'password', 'trim|required');
	$this->form_validation->set_rules('nama_toko', 'nama toko', 'trim|required');
	$this->form_validation->set_rules('alamat_toko', 'alamat toko', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');
	$this->form_validation->set_rules('foto', 'foto', 'trim|required');
	$this->form_validation->set_rules('created_at', 'created at', 'trim|required');
	$this->form_validation->set_rules('updated_at', 'updated at', 'trim|required');

	$this->form_validation->set_rules('username', 'username', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file PenggunaController.php */
/* Location: ./application/controllers/PenggunaController.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-07-06 14:02:19 */
/* http://harviacode.com */
