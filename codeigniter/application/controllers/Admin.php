<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
	      $this->load->library('datatables');
    }

    public function index()
    {
        if(empty($this->session->userdata('username')) or $this->session->userdata('status') != 'Admin')
        {
            redirect('login');
        }
        $this->load->view('admin/Dashboard');
    }

    public function productlist()
    {
        if(empty($this->session->userdata('username')) or $this->session->userdata('status') != 'Admin')
        {
            redirect('login');
        }
        $data['productlist'] = $this->ProductModel->get_all();
        $this->load->view('admin/Produk', $data);
    }

    public function categorylist()
    {
        if(empty($this->session->userdata('username')) or $this->session->userdata('status') != 'Admin')
        {
            redirect('login');
        }
        $data['categorylist'] = $this->CategoryModel->get_all();
        $this->load->view('admin/Category', $data);
    }

    public function memberlist()
    {
        if(empty($this->session->userdata('username')) or $this->session->userdata('status') != 'Admin')
        {
            redirect('login');
        }
        $data['memberlist'] = $this->PenggunaModel->get_all();
        $this->load->view('admin/Member', $data);
    }

    public function memberform()
    {
      if(empty($this->session->userdata('username')) or $this->session->userdata('status') != 'Admin')
      {
          redirect('login');
      }

      $data['username'] = $this->uri->segment(3);
      $data['jenis']    = "Member";

      if($data['username']           == "new"){
        $data['qry']                 = $data['username'];
        $data['username']            = "";
        $data['nama_lengkap']        = "";
        $data['email']               = "";
        $data['tgl_lahir']           = "";
        $data['jenis_kelamin']       = 1;
        $data['no_hp']               = "";
        $data['alamat_pengguna']     = "";
        $data['alamat_toko']         = "";
        $data['status']              = "";
        $data['foto']                = base_url('assets/images/pengguna/default.png');
        $data['nama_toko']           = "";
      }else{
        $data_pengguna               = $this->PenggunaModel->get_by_id($data['username']);
        $data['qry']                 = "update";
        $data['nama_lengkap']        = $data_pengguna->nama_lengkap;
        $data['email']               = $data_pengguna->email;
        $data['tgl_lahir']           = $data_pengguna->tgl_lahir;
        $data['jenis_kelamin']       = $data_pengguna->jenis_kelamin;
        $data['no_hp']               = $data_pengguna->no_hp;
        $data['alamat_pengguna']     = $data_pengguna->alamat_pengguna;
        $data['alamat_toko']         = $data_pengguna->alamat_toko;
        $data['status']              = $data_pengguna->status;
        $data['foto'  ]              = $this->Custom->photo($data_pengguna->foto, 'pengguna', "");
        $data['nama_toko']           = $data_pengguna->nama_toko;
      }
      $this->load->view('admin/MemberForm', $data);
    }

    public function logasmember()
    {
      if(empty($this->session->userdata('username')) or $this->session->userdata('status') != 'Admin')
      {
          redirect('login');
      }

      $cek['username']      = $this->uri->segment(3);
      $data_pengguna        = $this->PenggunaModel->get_by_id($cek['username']);

      if(!$data_pengguna)
      {
        $this->session->set_flashdata('kelas', 'warning!');
        $this->session->set_flashdata('status', 'Username tidak terdaftar!');
        redirect('admin/memberlist');
      }else{
        $cek['foto']         = $this->Custom->photo($data_pengguna->foto, 'pengguna', "");
        $cek['nama_lengkap'] = $data_pengguna->nama_lengkap;
        $cek['status']       = $data_pengguna->status;
        $this->session->set_userdata($cek);

        redirect('main/pengguna/'.$cek['username']);
      }
    }

    public function slidelist()
    {
      if(empty($this->session->userdata('username')) or $this->session->userdata('status') != 'Admin')
      {
          redirect('login');
      }

      $data['slidelist'] = $this->SlideModel->get_all();
      $this->load->view('admin/Slide', $data);
    }

    public function slideform()
    {
      if(empty($this->session->userdata('username')) or $this->session->userdata('status') != 'Admin')
      {
          redirect('login');
      }

      $this->load->view('admin/SlideForm');
    }

    public function profile()
    {
      if(empty($this->session->userdata('username')) or $this->session->userdata('status') != 'Admin')
      {
          redirect('login');
      }

      $data['username']            = $this->session->userdata('username');
      $data['jenis']               = "Profil";
      $data_pengguna               = $this->PenggunaModel->get_by_id($data['username']);
      $data['qry']                 = "update";
      $data['nama_lengkap']        = $data_pengguna->nama_lengkap;
      $data['email']               = $data_pengguna->email;
      $data['tgl_lahir']           = $data_pengguna->tgl_lahir;
      $data['jenis_kelamin']       = $data_pengguna->jenis_kelamin;
      $data['no_hp']               = $data_pengguna->no_hp;
      $data['alamat_pengguna']     = $data_pengguna->alamat_pengguna;
      $data['alamat_toko']         = $data_pengguna->alamat_toko;
      $data['status']              = $data_pengguna->status;
      $data['foto'  ]              = $this->Custom->photo($data_pengguna->foto, 'pengguna', "");
      $data['nama_toko']           = $data_pengguna->nama_toko;

      $this->load->view('admin/MemberForm', $data);
    }

    public function password()
    {
      if(empty($this->session->userdata('username')) or $this->session->userdata('status') != 'Admin')
      {
          redirect('login');
      }

      $this->load->view('admin/MemberPassForm');
    }
}
