<label id="error" style="color: Red; display: none; margin-bottom: 10px">* Input number</label>
<footer class="main-footer">
  <div class="pull-right hidden-xs">
    Theme by <strong><a href="http://almsaeedstudio.com">Almsaeed Studio</a></strong> <b>Version</b> 1.3.0
  </div>
  <strong>Copyright &copy; 2018 <a href="#">Asoy Corp</a>.</strong> All rights reserved.
</footer>
</div>

<?php if(!empty($this->session->flashdata('info'))) { ?>
<script type="text/javascript">
  $(document).ready(function(){
    $("#information").removeClass("hidden");
    $("#information").addClass("animated");
    $("#information").addClass("fadeIn");
    setTimeout(function () {
      $("#information").addClass("animated");
      $("#information").addClass("fadeOut");
        }, 4000
    );
    setTimeout(function () {
      $("#information").addClass("hidden");
        }, 4500
    );
  });
</script>
<?php } ?>
<script src="<?= base_url('assets/js/app.min.js') ?>"></script>
<script src="<?= base_url('assets/js/demo.js') ?>"></script>
<script src="<?= base_url('assets/js/dashboard.js') ?>"></script>

<script>
$(document).ready(function(){

$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});

  $image_crop = $('.cropfoto_member').croppie({
      enableExif: true,
      viewport: {
        width:220,
        height:270,
        type:'square' //circle
      },
      boundary:{
        width:300,
        height:350
      }
    });

  $crop_slide = $('.cropfoto_slide').croppie({
      enableExif: true,
      viewport: {
        width:980,
        height:450,
        type:'square' //circle
      },
      boundary:{
        width:1000,
        height:480
      }
    });


  $('#foto_member').on('change', function(){
      // $('.image_demo').show();
      // $('#crop').show();
      var reader = new FileReader();
      reader.onload = function (event) {
        $image_crop.croppie('bind', {
          url: event.target.result
        }).then(function(){
          console.log('jQuery bind complete');
        });
      }
      reader.readAsDataURL(this.files[0]);
      // $('#uploadimageModal').modal('show');
  });

  $('#foto_slide').on('change', function(){
      var reader = new FileReader();
      reader.onload = function (event) {
        $crop_slide.croppie('bind', {
          url: event.target.result
        }).then(function(){
          console.log('jQuery bind complete');
        });
      }
      reader.readAsDataURL(this.files[0]);
  });



  $('#uploadfoto_member').on('click', function (ev) {
  var url = $('#urlfoto_member').val();
  var username = $('#username').val();
  $image_crop.croppie('result', {
  type: 'canvas',
  size: 'viewport'
  }).then(function (resp) {
  $.ajax({
  url: url,
  type: "POST",
  data: {"image":resp,"username":username},
  success: function (result) {
    alert(result);
    // window.location = data.url;
  },
  error:function(result){
    console.log(result.responseText);
  }
  });
  });
  });

  $('#uploadfoto_slide').on('click', function (ev) {
  var url = $('#urlfoto_slide').val();
  $crop_slide.croppie('result', {
  type: 'canvas',
  size: 'viewport'
  }).then(function (resp) {
  $.ajax({
  url: url,
  type: "POST",
  data: {"image":resp},
  success: function (result) {
    window.location = result.url;
    // window.location = data.url;
  },
  error:function(result){
    console.log(result.responseText);
  }
  });
  });
  });

  $('#frm_member').on('submit', function(e){
    e.preventDefault();
    var url = $(this).attr("action");
    $image_crop.croppie('result', {
    type: 'canvas',
    size: 'viewport'
    }).then(function (resp) {
      var file_foto = $('#foto_member').val();
      if(file_foto==""){
        $('#foto').val('');
      }else{
        $('#foto').val(resp);
      }
      var form_data = $('#frm_member').serialize();
    $.ajax({
    url: url,
    type: "POST",
    data: form_data,
    success: function (result) {
     if(result.status == 0){
      alert(result.pesan);
      $('#fbform_member').html(result.pesan);
     }else{
      window.location = result.url;
     }
    },
    error:function(result){
      console.log(result.responseText);
    }
    });
    });
  })


});
</script>


</body>
</html>
