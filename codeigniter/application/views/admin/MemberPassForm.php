<?php $this->load->view('admin/Sidebar'); ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Password
      <small>Form</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#"><i class="fa fa-gear"></i> Pengaturan Akun</a></li>
      <li class="active">Password</li>
    </ol>
  </section>

  <section class="content">
      <div class="alert hidden" id="information">
          <h4>Informasi</h4>
          <span id="content-info"></span>
      </div>

      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form Ganti Password</h3>
            </div>
              <div class="box-body">
                <div class="form-group row">
                  <label for="passwordlama" class="col-sm-3 control-label"> Password Lama</label>
                  <div class="col-sm-9">
                    <input type="password" name="passwordlama" class="form-control" placeholder="Password Lama" id="pass_lama" autofocus required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="passwordbaru" class="col-sm-3 control-label"> Password Baru</label>
                  <div class="col-sm-9">
                    <input type="password" name="passwordbaru" class="form-control" placeholder="Password Baru" id="pass_baru" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="passwordulangbaru" class="col-sm-3 control-label"> Ulangi Password Baru</label>
                  <div class="col-sm-9">
                    <input type="password" name="passwordulangbaru" class="form-control" placeholder="Ulangi Password Baru" id="ulang_pass_baru" required>
                  </div>
                </div>
              </div>

              <div class="box-footer">
                <button class="btn btn-primary pull-right" onclick="changepass('<?= base_url("pengguna/changepass.html") ?>')" >Simpan</button>
              </div>
          </div>
        </div>
      </div>
  </section>
</div>
<script type="text/javascript">
  $(function(){
    $('.select2').select2();
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue'
    })
  });
</script>
<?php $this->load->view('admin/FootLayout') ?>
