<?php $this->load->view('admin/sidebar'); ?>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Kategori
      <small>Daftar</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
      <li><i class="fa fa-dropbox"></i> Produk</li>
      <li class="active">Daftar Kategori</li>
    </ol>
  </section>

  <section class="content">
    <?php if(!empty($this->session->flashdata('info'))){ ?>
    <div class="callout callout-<?= $this->session->flashdata('kelas') ?> hidden" id="information">
        <h4>Informasi</h4>
        <?= $this->session->flashdata('info') ?>
    </div>
    <?php } ?>

    <div class="row">
      <section class="col-md-12 connectedSortable">
        <div class="box box-primary">
          <div class="box-header">
            <i class="fa fa-users"></i>

            <h3 class="box-title">Kategori</h3>
            <div class="pull-right box-tools">
              <button type="button" class="btn btn-primary btn-sm" title="Add" data-toggle="modal" data-target="#modal-default" onclick="form_kategori('', '')">
                <i class="fa fa-plus"></i> Tambah Kategori</button>
              <button type="button" class="btn btn-danger btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <table id="table_id" class="display">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Kategori</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php $no = 1;
                    foreach ($categorylist as $category) {
                  ?>
                    <tr>
                        <td><?= $no ?></td>
                        <td><?=  $category->nama_kategori ?></td>
                        <td>
                          <button type="button" class="btn btn-warning btn-sm" title="Edit" data-toggle="modal" data-target="#modal-default" onclick="form_kategori('<?=  $category->kode_kategori ?>', '<?=  $category->nama_kategori ?>')">
                            <i class="fa fa-pencil"></i> Edit</button>
                          <button type="button" class="btn btn-danger btn-sm" title="Delete" data-toggle="modal" data-target="#modal-warning" onclick="hapus('<?=  $category->nama_kategori ?>', '<?=  base_url('/Category/categorydelete/'.$category->kode_kategori) ?>')">
                            <i class="fa fa-times"></i> Delete</button>
                        </td>
                    </tr>
                  <?php $no++; } ?>
                </tbody>
            </table>
          </div>
          <div class="box-footer clearfix">
          </div>
        </div>

      </section>
    </div>
  </section>
</div>

<div class="modal modal-primary fade" id="modal-warning">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Hapus Kategori <span class="nama"></span></h4>
      </div>
      <div class="modal-body">
        <p>Apakah benar, data kategori <strong><span class="nama"></span></strong> akan dihapus?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
        <a href="<?=  base_url('/admin/delete') ?>" class="btn btn-danger" id="btn_delete">Delete</a>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span id="jenis"></span> Kategori <span class="nama"></span></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" action="<?= base_url('category/store.html') ?>" method="post">
          <div class="box-body">
            <div class="form-group">
              <label for="nama_kategori" class="col-sm-3 control-label">Nama Kategori</label>
              <div class="col-sm-8">
                <input type="text" name="nama_kategori" value="" id="nama_kategori" class="form-control" placeholder="Nama Kategori" autofocus required>
                <input type="hidden" name="kode_kategori" value="" id="kode_kategori" class="form-control" placeholder="Kode Kategori">
              </div>
            </div>
          </div>

          <div class="box-footer">
            <input type="hidden" id="jenis_kategori">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
</script>

<?php $this->load->view('admin/FootLayout'); ?>
