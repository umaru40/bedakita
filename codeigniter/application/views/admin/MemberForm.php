<?php $this->load->view('admin/Sidebar') ?>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?= $jenis ?>
      <small>Form</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <?php if( $jenis == "Member") { ?>
      <li><a href="<?= base_url('/admin/memberlist.html') ?>"><i class="fa fa-users"></i> Member</a></li>
      <li class="active">Form Member</li>
      <?php }else{  ?>
      <li><a href="#"><i class="fa fa-gear"></i> Pengaturan Akun</a></li>
      <li class="active">Profil</li>
      <?php } ?>
    </ol>
  </section>

  <section class="content">
     <div class="row">
        <div class="col-md-4">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Foto Profil</h3>
            </div>

            <form role="form">
              <div class="box-body">
                <div class="form-group text-center">
                  <img class="profile-user-img img-responsive img-thumbnail" id="foto_memberx" src="<?= $foto ?>" alt="<?= $username ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Perbarui Foto Profil</label>

                  <input type="file" id="foto_member">
                </div>
              </div>

              <div class="cropfoto_member" style="width:350px; margin-top:30px"></div>

            </form>
          </div>
        </div>

        <div class="col-md-8">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Info <?= $jenis ?></h3>
            </div>
            <form action="<?= base_url('/pengguna/store.html') ?>" class="form-horizontal" method="post" id="frm_member">
              <div class="box-body">
                <div class="form-group">
                  <label for="username" class="col-sm-3 control-label">Username</label>
                  <div class="col-sm-9">
                    <input type="text" name="username" value="<?= $username ?>" class="form-control" id="username" placeholder="Username" maxlength="15" autofocus required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="nama_lengkap" class="col-sm-3 control-label">Nama Lengkap</label>
                  <div class="col-sm-9">
                    <input type="text" name="nama_lengkap" value="<?= $nama_lengkap ?>" class="form-control" placeholder="Nama Lengkap" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="nama_toko" class="col-sm-3 control-label">Nama Toko</label>
                  <div class="col-sm-9">
                    <input type="text" name="nama_toko" value="<?= $nama_toko ?>" class="form-control" placeholder="Nama Toko">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-3 control-label">Email</label>
                  <div class="col-sm-9">
                    <input type="email" name="email" value="<?= $email ?>" class="form-control" placeholder="Email" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="tgl_lahir" class="col-sm-3 control-label">Tanggal Lahir</label>
                  <div class="col-sm-9">
                    <input type="date" name="tgl_lahir" value="<?= $tgl_lahir ?>" class="form-control" placeholder="Tanggal Lahir" required>
                  </div>
                </div>
                <div class="form-group" >
                  <label for="jenis_kelamin" class="col-sm-3 control-label">Jenis Kelamin</label>
                  <div class="col-sm-4">
                    <input type="radio" name="jenis_kelamin" class="flat-red" value="1" <?= ($jenis_kelamin == 1 ? 'checked' : '') ?>> Laki Laki
                  </div>
                  <div class="col-sm-5">
                    <input type="radio" name="jenis_kelamin" class="flat-red" value="1" <?= ($jenis_kelamin == 2 ? 'checked' : '') ?>> Perempuan
                  </div>
                </div>
                <div class="form-group">
                  <label for="no_hp" class="col-sm-3 control-label">No HP</label>
                  <div class="col-sm-9">
                    <input type="text" name="no_hp" value="<?= $no_hp ?>" class="form-control" placeholder="No HP" onkeypress="return numeric(event);" maxlength="15">
                  </div>
                </div>
                <div class="form-group" >
                  <label for="alamat_pengguna" class="col-sm-3 control-label">Alamat Member</label>
                  <div class="col-sm-9">
                    <textarea name="alamat_pengguna" class="form-control" placeholder="Alamat Pengguna" rows="3"><?= $alamat_pengguna ?></textarea>
                  </div>
                </div>
                <div class="form-group" >
                  <label for="alamat_toko" class="col-sm-3 control-label">Alamat Toko</label>
                  <div class="col-sm-9">
                    <textarea name="alamat_toko" class="form-control" placeholder="Alamat Toko" rows="3"><?= $alamat_toko ?></textarea>
                  </div>
                </div>
                <div class="form-group" >
                  <label for="status" class="col-sm-3 control-label">Status</label>
                  <div class="col-sm-9">
                    <select class="form-control select2" name="status" style="width: 100%;">
                      <option value="" disabled selected>Pilih Status</option>
                      <option <?= ($status == "Admin" ? "selected" : "") ?>>Admin</option>
                      <option <?= ($status == "Member" ? "selected" : "") ?>>Member</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="box-footer">
                <div id="fbform_member"></div>
                <input type="hidden" id="foto" name="foto">
                <input type="hidden" name="qry" value="<?= $qry ?>">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
  </section>
</div>
<script type="text/javascript">
  $(function(){
    $('.select2').select2();
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue'
    })
  });
</script>
<?php $this->load->view('admin/FootLayout') ?>
