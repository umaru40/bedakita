<?php $this->load->view('main/HeadLayout') ?>
<section>

<div class="container">
	<div class="row">
		<div class="col-sm-12">

		<div class="category-tab shop-details-tab" style="margin:auto"><!--category-tab-->
			<div class="col-sm-12">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#details" data-toggle="tab">Biodata</a></li>
					<li><a href="#password" data-toggle="tab">Ubah Password</a></li>
					<li><a href="#profiltoko" data-toggle="tab">Daftar Produk Anda</a></li>
				</ul>
			</div>
			<div class="tab-content">
				<div class="tab-pane fade active in" id="details" >
					<div class="col-sm-4">
						<div class="row" id="photo_thumb">
							<img src="<?= $foto ?>" class="img img-responsive " width="70%" alt="<?=$pengguna->nama_lengkap?>">
						</div>
						<div class="row" style="padding-top:20px">
							<div class="col-sm-12">
								<b>Edit Foto Profil</b>
								<input type="file" class="btn btn-primary" id="upload_image">
								<div class="image_demo" style="width:350px; margin-top:30px"></div>
								<button class="btn btn-primary" id="crop">Unggah Gambar</button>
								<input type="hidden" name="username" id="username" value="<?=$pengguna->username?>">
								<input type="hidden" id="url_fotoPengguna" value="<?= base_url('/pengguna/updatefoto/'.$pengguna->username.'.html')?>">
							</div>
						</div>
					</div>
					<div class="col-sm-8">

					<div class="table-responsive cart_info">
						<form action="<?= base_url('/pengguna/updatebio/'.$pengguna->username.'.html')?>" method="post">
							<input type="hidden" name="_method" value="post">
						<table class="table table-condensed">
							<tbody>
								<tr >
									<td>Username</td>
									<td width="70%"><?=$pengguna->username?></td>
								</tr>
								<tr>
									<td >Nama Lengkap</td>
									<td ><?=$pengguna->nama_lengkap?></td>
								</tr>
								<tr>
									<td>Tanggal Lahir</td>
									<td width="70%"><?=$tgl_lahir?></td>
								</tr>
								<tr>
									<td>Jenis Kelamin</td>
									<td width="70%">
										<input type="radio" value="1" name="jenis_kelamin" <?= $pengguna->jenis_kelamin == 1?"checked":""?> > Laki-laki
										&nbsp;&nbsp;
										<input type="radio" value="2" name="jenis_kelamin" <?= $pengguna->jenis_kelamin == 2?"checked":""?> > Perempuan
									</td>
								</tr>
								<tr>
									<td>NO HP</td>
									<td width="70%">
										<input type="text" name="no_hp" value="<?=$pengguna->no_hp?>" class="form-control" maxlength="15" placeholder="No HP" onkeypress='return numeric(event);' required>
									</td>
								</tr>
								<tr>
									<td>Alamat Asli</td>
									<td width="70%">
										<input type="text" name="alamat_pengguna" value="<?=$pengguna->alamat_pengguna?>" class="form-control" placeholder="Alamat Asli" required>
									</td>
								</tr>

								<tr>
									<td>Nama Toko</td>
									<td width="70%">
										<input type="text" name="nama_toko" value="<?=$pengguna->nama_toko?>" class="form-control" placeholder="Nama Toko" required>
									</td>
								</tr>
                <tr>
									<td>Alamat Toko</td>
									<td width="70%">
										<input type="text" name="alamat_toko" value="<?=$pengguna->alamat_toko?>" class="form-control" placeholder="Alamat Toko" required>
									</td>
								</tr>
								<tr>
									<td></td>
									<td width="70%">
										<input type="submit" value="SIMPAN" class="btn btn-primary">
									</td>
								</tr>
							</tbody>
						</table>
						</form>
					</div>
					</div>

				</div>

				<div class="tab-pane fade" id="profiltoko" >
					<div class="container">
						<div class="row">
							<div class="col-sm-11" style="margin:auto;">
								<button class="btn btn-primary" id="btn_addProduk"><span class="fa fa-plus"></span> Tambah Produk</button>
								<br><br>
								<div class="cover_frm_produk" style="padding-bottom: 50px;">
								 	<div class="table-responsive cart_info">
								 		<div class="fb_frm_produk">
								 		</div>
                  <form action="<?= base_url('product/addproduk.html') ?>" method="post" id="frm_produk">

                    <div class="form-group row" style="margin-top:30px">
                      <label for="nama_produk" class="col-sm-3 control-label"> Nama Produk</label>
                      <div class="col-sm-8">
                        <input type="text" name="nama_produk" class="form-control" placeholder="Nama Produk" required>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-sm-3 control-label" for="harga_produk">Harga</label>
                      <div class="input-group col-sm-8" style="padding: 0 15px">
                        <div class="input-group-addon">Rp.</div>
                        <input type="text" name="harga_produk" class="form-control" placeholder="Harga Produk" onkeypress='return numeric(event)' required>
                        <div class="input-group-addon">.-</div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="kategori_produk" class="col-sm-3 control-label"> Kategori Produk</label>
                      <div class="col-sm-8">
                        <select class="form-control select2" name="kategori_produk" style="width: 100%">
                          <option value="" disabled selected> Pilih Kategori</option>
                          <?php foreach ($categorie as $kategori) { ?>
                            <option value="<?= $kategori->kode_kategori ?>"><?= $kategori->nama_kategori ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="deskripsi_produk" class="col-sm-3 control-label"> Deskripsi Produk</label>
                      <div class="col-sm-8">
                        <textarea name="deskripsi_produk" rows="3" cols="50" class="form-control editor" id="keterangan_produk"></textarea>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-sm-8 col-sm-offset-3">
                        <button type="submit" name="submit" class="btn btn-primary">Lanjutkan</button>
                      </div>
                    </div>

                  </form>
									</div>
								</div>
								<div id="cover_upload_gambar_produk" style="padding-bottom: 50px;">
									<center>
									<b>Unggah Gambar Produk</b>
									<input type="file" class="btn btn-primary" id="upload_image_produk">
									<div class="image_demo_produk" style="width:350px; margin-top:30px"></div>
									<input type="hidden" id="url_fotoProduk" value="<?= base_url('product/insertgambarproduk.html')?>">
									<button class="btn btn-primary" id="crop_gambar_produk">SELESAI</button>
									</center>
								</div>
								<table id="list_produk" class="table table-striped table-bordered" style="width:100%">
							        <thead>
							            <tr>
							                <th>Nama Produk</th>
							                <th>Harga Produk</th>
							                <th>Kategori</th>
							                <th>Tanggal Input</th>
							                <th>Action</th>
							            </tr>
							        </thead>
							        <tbody>
										<?php foreach($produk as $produk){ ?>
							            <tr>
							                <td><?=$produk->nama_produk?></td>
							                <td>Rp. <?= number_format($produk->harga_produk,0,'','.') ?>,-</td>
							                <td><?=$produk->nama_kategori?></td>
							                <td><?=$tgl_pro[$produk->kode_produk]?></td>
							                <td>
							                	<center>

							                	<a href="<?= base_url('main/editproduk/'.$produk->kode_produk.'.html')?>" class="btn btn-success" style="padding-left: 10px;"><span class="fa fa-pencil"></span> Edit</a>
                                <a href="<?= base_url('product/deleteproduk/'.$produk->kode_produk.'.html')?>" class="btn btn-danger" onclick="hapus_produk('btn<?= $produk->kode_produk ?>')" id="btn<?= $produk->kode_produk ?>"><span class="fa fa-trash-o"></span> Hapus</a>
							                	</center>
							                </td>
							            </tr>
										<?php } ?>
							        </tbody>
							    </table>
							</div>
						</div>
					</div>
				</div>

        <div class="tab-pane fade" id="password" >
					<div class="container">
            <h4 class="modal-title">Ganti Password</h4>

            <div class="callout hidden" id="information" style="padding:20px">
                <h4>Informasi</h4>
                <span id="content-info"></span>
            </div>

            <div class="form-group row" style="margin-top:30px">
              <label for="passwordlama" class="col-sm-3 control-label"> Password Lama</label>
              <div class="col-sm-8">
                <input type="password" name="passwordlama" class="form-control" placeholder="Password Lama" id="pass_lama" autofocus required>
              </div>
            </div>
            <div class="form-group row">
              <label for="passwordbaru" class="col-sm-3 control-label"> Password Baru</label>
              <div class="col-sm-8">
                <input type="password" name="passwordbaru" class="form-control" placeholder="Password Baru" id="pass_baru" required>
              </div>
            </div>
            <div class="form-group row">
              <label for="passwordulangbaru" class="col-sm-3 control-label"> Ulangi Password Baru</label>
              <div class="col-sm-8">
                <input type="password" name="passwordulangbaru" class="form-control" placeholder="Ulangi Password Baru" id="ulang_pass_baru" required>
              </div>
            </div>

            <div class="form-group row">
              <div class="col-sm-9 col-sm-offset-3">
                <button type="button" class="btn btn-success" onclick="changepass('<?= base_url("pengguna/changepass.html") ?>')" >Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
					</div>
				</div>
			</div>
	   </div><!--/category-tab-->
		</div>
	</div>
</div>
</section>

<div id="modal_editproduk" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Hapus Produk</h4>
      </div>
      <div class="modal-body text-center">
      	<input type="hidden" id="kode_produkHapus">
        <p>Data Yang Terhapus Tidak Akan Bisa Dikembalikan lagi</p>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-default">OK</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php $this->load->view('main/FootLayout') ?>
