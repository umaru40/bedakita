<?php $this->load->view('main/HeadLayout') ?>
<section>
	<div class="container" id="content">
		<div class="row">
			<div class="col-sm-12 padding-right">

				<table id="list_produk" class="table table-striped table-bordered" style="width:100%">
			        <thead>
			            <tr>
			                <th>NAMA BEDAG</th>
			                <th>ALAMAT BEDAG</th>
			                <th>PEMILIK</th>
			                <th>CONTACT PERSON</th>
			                <th>LIHAT PRODUK</th>
			            </tr>
			        </thead>
			        <tbody>
						<?php foreach($daftarbedag as $bedag) { ?>
			            <tr>
			                <td><?= $bedag->nama_toko ?></td>
			                <td><?= $bedag->alamat_toko ?></td>
			                <td><?= $bedag->nama_lengkap ?></td>
			                <td><?= $bedag->no_hp ?></td>
			                <td><a href="<?= base_url('main/produk/'.$bedag->username.'.html')?>" class="btn btn-success"><i>Lihat Produk</i></a></td>
			            </tr>
						<?php } ?>
			        </tbody>
			    </table>

			</div>
		</div>
	</div>
</section>
<?php $this->load->view('main/FootLayout') ?>
