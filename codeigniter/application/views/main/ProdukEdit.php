<?php $this->load->view('main/HeadLayout') ?>
<section>

<div class="container">
	<div class="row">
		<div class="col-sm-12">

		<div class="category-tab shop-details-tab" style="margin:auto"><!--category-tab-->
			<div class="col-sm-12">
				<!-- <ul class="nav nav-tabs">
					<li ><a href="#details" data-toggle="tab">Biodata</a></li>
				</ul> -->
			</div>
			<div class="tab-content">
				<div class="tab-pane fade active in" id="details" >
					<div class="col-sm-12" style="margin-bottom:20px">
					<a href="<?= base_url('/main/pengguna.html') ?>" class="btn btn-success text-primary"><span class="fa fa-reply-all"></span><i> Kembali</i></a>
					</div>

					<div class="col-sm-4">
            <div class="row" id="photo_thumb">
						  <img id="img_editproduk" src="<?= $foto ?>" class="img img-responsive " width="70%" alt="<?= $product->nama_produk ?>">
            </div>
            <div class="col-sm-12">
  						<b>Edit Gambar Produk</b>
  						<input type="file" class="btn btn-primary" id="upload_imageEditProduk">
  						<input type="hidden" id="url_editprodukgbr" value="<?= base_url('/pengguna/edit_produkgbr.html')?>">
  						<div class="gbr_produk" style="width:350px; margin-top:30px"></div>
  						<div id="edit_produk"></div>
  						<button class="btn btn-primary" id="crop_gbrproduk">Unggah Gambar</button>
  					</div>
					</div>
					<div class="col-sm-8">

					<div class="table-responsive cart_info">
            <form action="<?= base_url('product/simpaneditproduk.html') ?>" method="post" id="frm_editproduk">
						<input type="hidden" name="kode_produk" id="kode_produk" value="<?=$product->kode_produk?>">

            <div class="form-group row" style="margin-top:30px">
              <label for="nama_produk" class="col-sm-3 control-label"> Nama Produk</label>
              <div class="col-sm-8">
                <input type="text" name="nama_produk" value="<?= $product->nama_produk ?>" class="form-control" placeholder="Nama Produk" required>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-3 control-label" for="harga_produk">Harga</label>
              <div class="input-group col-sm-8" style="padding: 0 15px">
                <div class="input-group-addon">Rp.</div>
                <input type="text" name="harga_produk" value="<?= $product->harga_produk ?>" class="form-control" placeholder="Harga Produk" onkeypress='return numeric(event)' required>
                <div class="input-group-addon">.-</div>
              </div>
            </div>

            <div class="form-group row">
              <label for="kategori_produk" class="col-sm-3 control-label"> Kategori Produk</label>
              <div class="col-sm-8">
                <select class="form-control select2" name="kategori_produk" style="width: 100%">
                  <option value="" disabled selected> Pilih Kategori</option>
                  <?php foreach ($categorie as $kategori) { ?>
                    <option value="<?= $kategori->kode_kategori ?>" <?= ($product->kode_kategori == $kategori->kode_kategori ? "selected" : "")?>><?= $kategori->nama_kategori ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label for="deskripsi_produk" class="col-sm-3 control-label"> Deskripsi Produk</label>
              <div class="col-sm-8">
                <textarea name="deskripsi_produk" rows="3" cols="50" class="form-control editor" id="keterangan_produk"><?= $product->deskripsi_produk ?> </textarea>
              </div>
            </div>

            <div class="form-group row">
              <div class="col-sm-8 col-sm-offset-3">
                <div id="fb_editproduk" class="text-primary"></div>
                <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
              </div>
            </div>
            </form>
					</div>
					</div>
				</div>
			</div>
		</div><!--/category-tab-->
		</div>
	</div>
</div>
</section>
<?php $this->load->view('main/FootLayout') ?>
