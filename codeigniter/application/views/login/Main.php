<?php $this->load->view('login/HeadLayout') ?>

<section id="form"><!--form-->
    <?php if(!empty($this->session->flashdata('status'))){ ?>
    <div class="row">
      <div class="col-sm-4 col-sm-offset-4">
        <div class="alert alert-info" role="alert">
          <?= $this->session->flashdata('status') ?>
        </div>
      </div>
    </div>
    <?php } ?>

		<div class="row">
			<div class="col-sm-4 col-sm-offset-4">
				<div class="login-form"><!--login form-->
					<h2>Masuk ke Bedag Malang</h2>
          <form action="<?= base_url('login/signin.html') ?>" method="post">
            <input type="text" name="username" class="form-control" value="<?= $this->session->flashdata('username') ?>" placeholder="Username" autofocus>
            <input type="password" name="password" class="form-control" placeholder="Password">
            <center><button type="submit" name="submit" class="btn btn-primary">Login</button></center>
          </form>
				</div>
			</div>
		</div>
</section>

<?php $this->load->view('login/FootLayout') ?>
