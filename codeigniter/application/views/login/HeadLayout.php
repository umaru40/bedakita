<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> BEDAGMALANG</title>

    <link href="<?= base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/font-awesome.min.css')?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/prettyPhoto.css')?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/price-range.css')?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/animate.css')?>" rel="stylesheet">
  	<link href="<?= base_url('assets/css/main.css')?>" rel="stylesheet">
  	<link href="<?= base_url('assets/css/responsive.css')?>" rel="stylesheet">
	  <link rel="stylesheet" href="<?= base_url('assets/css/datatables/datatables.css') ?>">
	  <link rel="stylesheet" href="<?= base_url('assets/css/datatables/DataTables-1.10.16/css/dataTables.bootstrap.css') ?>">
  	<link rel="stylesheet" href="<?= base_url('assets/css/croppie.css')?>">
  	<link rel="stylesheet" href="<?= base_url('assets/css/summernote.css')?>">
  	<link href="<?= base_url('assets/css/style.css')?>" rel="stylesheet">
</head>

<body>
	<header id="header"><!--header-->

		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<h3>BEDAG<span>MALANG</span></h3>
						</div>
					</div>
					<div class="col-sm-8">
					</div>
				</div>
			</div>
		</div><!--/header-middle-->

		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
				</div>
			</div>
		</div>
	</header>
