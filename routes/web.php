<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MainControl@index');
Route::get('/login.html','PenggunaController@index');
Route::post('/in.html','PenggunaController@in');
Route::get('/member/{id}','PenggunaController@show');
Route::post('/member/updateBio/{id}','PenggunaController@update');
Route::post('/member/updateFoto/{id}','PenggunaController@updateFoto');
Route::post('/member/gantipassword','PenggunaController@gantipassword');


Route::get('/admin.html','AdminController@index');
Route::get('/admin/profile/{username}.html','PenggunaController@profile');
Route::get('/admin/password.html','PenggunaController@passwordform');
Route::get('/admin/productlist.html','ProductController@index');
Route::get('/admin/productdelete/{kode_produk}','ProductController@productdelete');
Route::post('/admin/productinfo','ProductController@product_info');

Route::get('/admin/categorylist.html','CategoryController@index');
Route::get('/admin/categoryform/{kode_kategori}.html','CategoryController@categoryform');
Route::get('/admin/categorydelete/{kode_kategori}','CategoryController@categorydelete');
Route::post('/admin/categoryform/store.html','CategoryController@store');

Route::get('/admin/memberlist.html','PenggunaController@memberlist');
Route::get('/admin/memberform/{username}.html','PenggunaController@memberform');
Route::get('/admin/memberdelete/{username}','PenggunaController@memberdelete');
Route::post('/admin/memberform/store.html','PenggunaController@store');

Route::get('/admin/slidelist.html','SlideController@index');
Route::get('/admin/slideform/{id}.html','SlideController@slideform');
Route::get('/admin/slidedelete/{id}','SlideController@slidedelete');
Route::post('/admin/slideform/store.html','SlideController@store');


Route::post('/cari','PenjualanControl@cari');
Route::get('/detailproduk/{id}','PenjualanControl@detailproduk');
Route::post('/insertGambarProduk','PenjualanControl@insertGambarProduk');

Route::get('/daftarbedag', 'PenjualanControl@daftarbedag');
Route::get('/lihatproduk/{id}','PenjualanControl@lihatproduk');
Route::get('/produk/{username}','MainControl@product');

Route::post('/add_produk','PenjualanControl@add_produk');
Route::get('/editproduk/{id}','PenjualanControl@editproduk');
Route::post('/simpan_editproduk','PenjualanControl@simpan_editproduk');
Route::post('/edit_produkgbr','PenjualanControl@edit_produkgbr');
Route::post('/deleteproduk/{id}', 'PenjualanControl@deleteproduk');

Route::get('/signout', function(){
	Session::flush();
	return redirect()->to('/');
});
