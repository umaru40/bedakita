<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Pengguna;
use App\Models\Categorie;


class PenjualanControl extends Controller
{
    //
    public function cari(Request $request){
    	$cari              = $request->cari;
    	$product           = Product::where('nama_produk','like','%'.$cari.'%')->get();
        $data['product']   = $product;
        $data['categorie'] = Categorie::all();
    	return view('display1',$data);
    }

    public function detailproduk($id)
    {
        $data['product']    = Product::where('kode_produk', $id)->first();
        $username           = $data['product']->username;
        $data['pengguna']   = Pengguna::where('username', $username)->first();
        $data['categorie']  = Categorie::all();
        $data['foto']       = Pengguna::photo('_produk', $data['product']->foto_produk);

    	  return view('detailproduk',$data);
    }

    public function add_produk(Request $request)
    {
        $username = Session::get('username');

        $insert_produk = Product::create([
            'nama_produk'       => $request->nama_produk,
            'username'          => $username,
            'deskripsi_produk'  => $request->deskripsi_produk,
            'harga_produk'      => $request->harga_produk,
            'kode_kategori'     => $request->kategori_produk,
            'foto_produk'       => 'default.png',

        ]);

        if($insert_produk)
        {
            $feedback['pesan']          = "Produk Berhasil Ditambahkan";
            $feedback['previous_url']   = url()->previous();
        }
        else
        {
            $feedback['pesan'] = "Terjadi Kesalahan ";
        }
        return $feedback;
    }

    public function insertGambarProduk(Request $request)
    {
        $username  = $request->username;
        $produk    = Product::where('username',$username)->latest()->first();

        $data = $request->image;
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);
        $image_name= $produk['kode_produk'].'.png';
        $path = public_path() . "/images/_produk/" . $image_name;

        if(file_put_contents($path, $data)){
            Product::where('kode_produk',$produk['kode_produk'])->update(['foto_produk'=>$image_name]);
            $feedback['pesan']  = 'Berhasil';
            $feedback['foto']   = $image_name;
        }else{
            $feedback['pesan'] = 'Gagal';
        }
        $feedback['url']   = url('/member/'.$username);
        return $feedback;
    }

    public function daftarbedag()
    {
        $data['daftarbedag']    = Pengguna::except_admin();
        $data['categorie']      = Categorie::all();
        return view('daftarbedag',$data);
    }

    public function lihatproduk($id)
    {
        if($id == "all")
        {
          $data['product'] = Product::all();
        }else{
          $data['product'] = Product::where('username', $id)->get();
        }

        $data['categorie'] = Categorie::all();
        return view('display1',$data);
    }

    public function editproduk($id)
    {
        $data['product']     = Product::where('kode_produk',$id)->first();
        $data['categorie']   = Categorie::all();
        $data['kategori']    = array();
        $data['kategori'][0] = 'Pilih Kategori';
        $data['foto']        = Pengguna::photo('_produk', $data['product']->foto_produk);

        foreach ($data['categorie'] as $category) {
          $data['kategori'][$category->kode_kategori] = $category->nama_kategori;
        }

        return view('member/editproduk', $data);
    }

    public function simpan_editproduk(Request $request)
    {
        $kode_produk = $request->kode_produk;

        $rule['nama_produk']      = 'required';
        $rule['harga_produk']     = 'required|numeric';
        $rule['kode_kategori']    = 'required';
        $rule['deskripsi_produk'] = 'required';
        $msg_error['required']    = 'Tidak boleh kosong';
        $msg_error['numeric']     = 'Hanya boleh diisi dengan angka saja';
        $this->validate($request,$rule,$msg_error);

        $data['nama_produk']      = $request->nama_produk;
        $data['harga_produk']     = $request->harga_produk;
        $data['kode_kategori']    = $request->kode_kategori;
        $data['deskripsi_produk'] = $request->deskripsi_produk;

        $qry = Product::where('kode_produk',$kode_produk)->update($data);
        $feedback['url'] = url('/editproduk/'.$kode_produk);
        if($qry){
            $feedback['pesan']  = "<p class='alert alert-info'>Berhasil !</p>";
            $feedback['gagal']  = 'Gagal';
        }

        return redirect()->back();
    }

    public function edit_produkgbr(Request $request)
    {
        $kode_produk          = $request->kode_produk;
        $data                 = $request->image;
        list($type, $data)    = explode(';', $data);
        list(, $data)         = explode(',', $data);
        $data                 = base64_decode($data);
        $image_name           = $kode_produk.'.png';
        $path                 = public_path() . "/images/_produk/" . $image_name;

        if(file_put_contents($path, $data)){
            Product::where('kode_produk',$kode_produk)->update(['foto_produk'=>$image_name]);
            $feedback['pesan']  = "<p class='alert alert-info'>Berhasil</p>";
            $feedback['foto']   = asset('/images/_produk/'.$image_name);
        }else{
            $feedback['pesan'] = 'Gagal';
        }
        $feedback['url']   = url('/editproduk/'.$kode_produk);
        return $feedback;
    }

    public function deleteproduk($id)
    {
        Product::where('kode_produk',$id)->delete();
    }


}
