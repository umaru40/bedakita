<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Image;
use App\Models\Pengguna;
use App\Models\Product;
use App\Models\Categorie;

class PenggunaController extends Controller
{
    public function index()
    {
        return view('login/Login');
    }

    public function in(Request $request)
    {
        $cek['username']      = $request->username;
        $cek['password']      = Pengguna::encry($request->password);
        $data_pengguna        = Pengguna::where('username', $cek['username'])->first();

        if(!$data_pengguna)
        {
          $request->session()->flash('info', 'Username tidak terdaftar!');
          return redirect('login.html');
        }else{
          if($data_pengguna->password == $cek['password'])
          {
            $cek['foto']      = Pengguna::photo('pengguna', $data_pengguna->foto);
            $request->session()->put('nama_lengkap', $data_pengguna->nama_lengkap);
            $request->session()->put('username', $data_pengguna->username);
            $request->session()->put('status', $data_pengguna->status);
            $request->session()->put('foto', $cek['foto']);

            if(Session::get('status') <> 'Admin'){
                return redirect()->to('/member/'.Session::get('username'));
            }else{
                return redirect('admin.html');
            }

          }else{
            $request->session()->flash('status', 'Password anda salah!');
            $request->session()->flash('username', $cek['username']);
            return redirect('login.html');
          }
        }
    }

    public function show($id)
    {
        if(!Session::get('username'))
        {
            return redirect()->to('/');
        }
        $username         = $id;
        $data['pengguna'] = Pengguna::where('username',$username)->first();
        $data['produk']   = Product::get_by_username($username);
        $data['categorie']= Categorie::all();
        $tanggal          = date_create($data['pengguna']->tgl_lahir);
        $data['tgl_lahir']= date_format($tanggal, "d M Y");
        $data['foto']     = Pengguna::photo('pengguna', $data['pengguna']->foto);
        //$data['foto']     = Storage::files( asset('images/pengguna/'.$data['pengguna']->foto));
        $data['tgl_pro']  = array();
        $data['kategori'] = array();
        $data['kategori'][0] = 'Pilih Kategori';

        foreach ($data['produk'] as $product) {
          $tgl_produk     = date_create($product->created_at);
          $data['tgl_pro'][$product->kode_produk] = date_format($tgl_produk, "H:i:s, d M Y");
        }

        foreach ($data['categorie'] as $category) {
          $data['kategori'][$category->kode_kategori] = $category->nama_kategori;
        }

        //print_r($data['foto']);
        return view('member/index',$data);
    }

    public function update(Request $request, $id)
    {
        $username                   = $id;

        $data['jenis_kelamin']      = 'required';
        $data['no_hp']              = 'required|numeric';
        $data['alamat_pengguna']    = 'required';
        $data['alamat_toko']        = 'required';
        $data['nama_toko']          = 'required';
        $msg_error['required']      = 'Tidak boleh kosong';
        $msg_error['numeric']       = 'Hanya boleh diisi dengan angka saja';

        $this->validate($request,$data,$msg_error);

        $data['jenis_kelamin']      = $request->jenis_kelamin;
        $data['no_hp']              = $request->no_hp;
        $data['alamat_pengguna']    = $request->alamat_pengguna;
        $data['alamat_toko']        = $request->alamat_toko;
        $data['nama_toko']          = $request->nama_toko;

        Pengguna::where('username',$username)->update($data);
        return redirect()->back();
    }

    public function updateFoto(Request $request, $id)
    {

        $data = $request->image;
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);
        $image_name= $id.'.png';
        $path = public_path() . "/images/pengguna/" . $image_name;

        if(file_put_contents($path, $data)){
            Pengguna::where('username',$id)->update(['foto'=>$image_name]);
            $feedback['pesan']  = 'Berhasil';
            $feedback['foto']   = $image_name;
        }else{
            $feedback['pesan'] = 'Gagal';
        }
        $feedback['url']   = url('/member/'.$id);
        return $feedback;
    }

    public function store(Request $request)
    {
      $data['username']            = $request->username;
      $data['nama_lengkap']        = $request->nama_lengkap;
      $data['email']               = $request->email;
      $data['tgl_lahir']           = $request->tgl_lahir;
      $data['tgl_daftar']          = NULL;
      $data['jenis_kelamin']       = $request->jenis_kelamin;
      $data['no_hp']               = $request->no_hp;
      $data['alamat_pengguna']     = $request->alamat_member;
      $data['alamat_toko']         = $request->alamat_toko;
      $data['status']              = $request->status;
      $data['nama_toko']           = $request->nama_toko;
      $data['password']            = Pengguna::encry($request->username);
      $data['foto']                = $request->foto;
      $qry                         = $request->qry;
      $current = Pengguna::where('username',$data['username'])->first();

       if($data['foto']             == ''){
          $image_name                = "default.jpg";
        }else{
          $crop = $data['foto'];
          list($type, $crop)         = explode(';', $crop);
          list(, $crop)              = explode(',', $crop);
          $crop                      = base64_decode($crop);
          $image_name                = $data['username'].'.png';
          $path                      = public_path() . "/images/pengguna/" . $image_name;
          file_put_contents($path, $crop);
        }

      if($qry == "new"){
        $data['foto'] = $image_name;

        if($data['username'] == $current['username'])
        {
          $feedback['status']  = 0;
          $feedback['pesan']   = "<div class='alert alert-warning'>Username sudah dipakai</div>";
        }else{
          $feedback['status']  = 1;
          $feedback['pesan']   = "<div class='alert alert-info'>Berhasil </div>";
          Pengguna::create($data);
          $request->session()->flash('info', 'Data Berhasil Ditambahkan ');
        }

      }else{

        if($data['foto']==''){
          $data['foto'] = $current['foto'];
        }else{
          $data['foto'] = $image_name;
        }
        $feedback['status']  = 1;
        $feedback['pesan']   = "<div class='alert alert-info'>Berhasil </div>";
        Pengguna::where('username',$data['username'])->update($data);
        $request->session()->flash('info', 'Data Berhasil Dirubah');
      }
      $feedback['url']     = url('admin/memberform/'.$data['username'].'.html');

      return $feedback;
    }

    public function storelo(Request $request)
    {
        $data['username']            = $request->username;
        $data['nama_lengkap']        = $request->nama_lengkap;
        $data['email']               = $request->email;
        $data['tgl_lahir']           = $request->tgl_lahir;
        $data['tgl_daftar']          = NULL;
        $data['jenis_kelamin']       = $request->jenis_kelamin;
        $data['no_hp']               = $request->no_hp;
        $data['alamat_member']       = $request->alamat_member;
        $data['alamat_toko']         = $request->alamat_toko;
        $data['status']              = $request->status;
        $data['nama_toko']           = $request->nama_toko;
        $data['password']            = Pengguna::encry($request->username);
        $data['foto']                = $request->foto;
        $recordcount                 = Pengguna::where('username',$data['username'])->count();


        if($data['foto']             == 'default.jpg'){
          $image_name                = $data['foto'];
        }else{
          $crop = $data['foto'];
          list($type, $crop)         = explode(';', $crop);
          list(, $crop)              = explode(',', $crop);
          $crop                      = base64_decode($crop);
          $image_name                = $data['username'].'.png';
          $path                      = public_path() . "/images/pengguna/" . $image_name;
          file_put_contents($path, $crop);
        }

        if($recordcount > 0){

        }else{

        }

        $feedback['pesan'] = "<div class='alert alert-info'>Berhasil !</div>";
        $feedback['url']   = url('admin/memberform/'.$data['username'].'.html');

        // return redirect()->to('admin/memberform/'.$data['username'].'.html');
        return $feedback;
    }

    public function store_foto(Request $request)
    {

        return $request->image;
    }

    public function memberlist()
    {
      if(!Session::get('username') or Session::get('status') <> 'Admin')
  		{
  		    return redirect()->to('/');
  		}

      $data['memberlist'] = Pengguna::all();
      return view('admin/Member', $data);
    }

    public function memberform($username)
    {
      $data['username']              = $username;
      $data['member']                = 'active';
      $data['profil']                = '';
      $data['jenis']                 = 'Member';

      if($data['username']           == "new"){
        $data['qry']                 = $data['username'];
        $data['username']            = "";
        $data['nama_lengkap']        = "";
        $data['email']               = "";
        $data['tgl_lahir']           = "";
        $data['tgl_daftar']          = "";
        $data['jenis_kelamin']       = "";
        $data['no_hp']               = "";
        $data['alamat_pengguna']     = "";
        $data['alamat_toko']         = "";
        $data['status']              = "Member";
        $data['foto']                = asset('images/pengguna/default.jpg');
        $data['nama_toko']           = "";
      }else{
        $data_pengguna               = Pengguna::where('username', $data['username'])->first();
        $data['qry']                 = "update";
        $data['nama_lengkap']        = $data_pengguna->nama_lengkap;
        $data['email']               = $data_pengguna->email;
        $data['tgl_lahir']           = $data_pengguna->tgl_lahir;
        $data['tgl_daftar']          = $data_pengguna->tgl_daftar;
        $data['jenis_kelamin']       = $data_pengguna->jenis_kelamin;
        $data['no_hp']               = $data_pengguna->no_hp;
        $data['alamat_pengguna']     = $data_pengguna->alamat_pengguna;
        $data['alamat_toko']         = $data_pengguna->alamat_toko;
        $data['status']              = $data_pengguna->status;
        $data['foto'  ]              = Pengguna::photo('pengguna', $data_pengguna->foto);
        $data['nama_toko']           = $data_pengguna->nama_toko;
      }

      return view('admin/MemberForm', $data);
    }

    public function memberdelete($username, Request $request)
    {
      Pengguna::where('username', $username)->delete();
      $request->session()->flash('info', 'Data <strong>'.$username.'</strong> telah dihapus!');
      return redirect('admin/memberlist.html');
    }

    public function profile($username)
    {
      $data['username']     = $username;
      $data_pengguna        = Pengguna::where('username', $data['username'])->first();
      $data['nama_lengkap'] = $data_pengguna->nama_lengkap;
      $data['email']        = $data_pengguna->email;
      $data['tgl_lahir']    = $data_pengguna->tgl_lahir;
      $data['tgl_daftar']   = $data_pengguna->tgl_daftar;
      $data['jenis_kelamin']= $data_pengguna->jenis_kelamin;
      $data['no_hp']        = $data_pengguna->no_hp;
      $data['alamat']       = $data_pengguna->alamat;
      $data['status']       = $data_pengguna->status;
      $data['foto'  ]       = Pengguna::photo('pengguna', $data_pengguna->foto);
      $data['nama_toko']    = $data_pengguna->nama_toko;

      $data['member']       = '';
      $data['profil']       = 'active';
      $data['jenis']        = 'Profil';

      return view('admin/MemberForm', $data);
    }

    public function passwordform()
    {
      return view('admin/MemberPassForm');
    }

    public function gantipassword(Request $request)
    {

        $rule['pass_lama']  = 'required';
        $rule['pass_baru']  = 'required';
        $msg_error['required']      = 'Tidak boleh kosong';
        $this->validate($request,$rule,$msg_error);


        $username   = Session::get('username');
        $pass_lama  = Pengguna::encry($request->pass_lama);
        $pass_baru  = Pengguna::encry($request->pass_baru);
        $count      = Pengguna::where('username',$username)
                              ->where('password',$pass_lama)->count();
        if($count == 1)
        {
            $data['password'] = $pass_baru;
            Pengguna::where('username',$username)
                    ->where('password',$pass_lama)
                    ->update($data);
            $feedback['status'] = "sukses";
            $feedback['pesan']  = "<p class='text-primary'>* Password Berhasil Dirubah</p>";
            $feedback['url']    = url('/member/'.$username);
        }else{
            $feedback['status'] = "gagal";
            $feedback['pesan']  = "<p class='text-danger'>* Gagal</p>";
        }

        return json_encode($feedback);

    }
}
