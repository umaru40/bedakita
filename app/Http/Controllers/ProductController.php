<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Product;
use App\Models\Pengguna;

class ProductController extends Controller
{
    public function index()
    {
      if(!Session::get('username') or Session::get('status') <> 'Admin')
  		{
  		    return redirect()->to('/');
  		}

      $data['productlist'] = Product::get_all();
      return view('admin/Produk', $data);
    }

    public function productdelete($kode_produk, Request $request)
    {
      $data_product        = Product::where('kode_produk', $kode_produk)->first();
      Product::where('kode_produk', $kode_produk)->delete();
      $request->session()->flash('info', 'Data <strong>'.$data_product->nama_produk.'</strong> telah dihapus!');
      $request->session()->flash('kelas', 'warning');
      return redirect('admin/productlist.html');
    }

    public function product_info(Request $request)
    {
      $product             = Product::get_by_code($request->kode_produk);
      $foto                = Pengguna::photo('_produk', $product->foto_produk);
      $data                = $product->nama_produk."|".
                              $product->username."|".
                              $product->nama_toko."|".
                              $product->deskripsi_produk."|Rp. ".
                              number_format($product->harga_produk,0,'','.').",-|".
                              $product->nama_kategori."|".
                              $foto."|";
      echo $data;
    }
}
