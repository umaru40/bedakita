<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class Pengguna extends Model
{
    protected $guarded = ['created_at'];

    public static function except_admin()
    {
      return DB::table('penggunas')
              ->where('status', '<>', 'Admin')
              ->get();
    }

    public static function photo($folder, $photo)
    {
      $separator = "\ ";
      $separator = substr($separator, 0, 1);
  		$thumb     = public_path()."\images".$separator.$folder.$separator.$photo;

  		 if(file_exists($thumb)){
        $thumb   = asset('images/'.$folder.'/'.$photo);
       }else{
  		 	$thumb   = asset('images/'.$folder.'/default.png');
  		 }

      return $thumb;
    }

    public static function encry($input)
    {
      return md5(substr(md5(substr(md5($input),0,9).'b9726dc'.substr(md5($input),9)),0,27).'f213a09b'.substr(md5(substr(md5($input),0,9).'b9726dc'.substr(md5($input),9)),27));
    }

    public static function pin($input)
    {
      return Crypt::encryptString($input);
    }

    public static function unpin($input)
    {
      return Crypt::decryptString($input);
    }
}
